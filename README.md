## Idea

Build the most artful and fancy clock using a Raspberry Pi, a screen and 24h 
video showing real people changing a 2m clock out of a wooden seven segment 
display.

Inspired by the [Standard Time Project](https://www.standard-time.com/about_en.php), 
which I discovered 15 years ago in a  small cinema at the heart of Berlin 
my goal was to have it running as a clock on my desk.

-> [See it in action](https://twitter.com/ju_key/status/1119161788640022528)

## Installation

### Prerequisites

- A Raspberry 3 Model B (I used an USB keyboard to run commands but an ssh connection also would do)
- An USB memory stick for hosting the video source file 
- A screen and stand (I am using a [Kuman 5 inch Resistive Touch Screen](http://www.lcdwiki.com/5inch_HDMI_Display) with Protective Case 800x480)
- The Standard Time Video which could be bought [here as Data DVD and for Download](https://www.standard-time.com/shop_en.php)


After connecting the screen and assembling the frame the Raspberry needs to be prepared.

### RaspberryPie

- [Raspbian Download page](https://www.raspberrypi.org/downloads/raspbian/)
- [Installation Guide and Raspbian manual](https://www.raspberrypi.org/documentation/raspbian/)

After the initial installation is done the USB stick containing the source video 
(in my case `st24.mov`) should be connected to the device. It will be mounted in 
`/media/pi/<name-of-your-usb-stick-partition>`. [MPlayer](https://en.wikipedia.org/wiki/MPlayer) 
will be used to serve the video. It can be installed by running the following 
commands in the terminal:

```bash
sudo apt update
sudo apt install mplayer
```

Now create a bash script to run mplayer starting using the current system time as offset:

```bash
cd ~
nano st.sh
```
The script should look like this:
```bash
#!/bin/bash
mplayer -ss $(($(date +%H)*3600+$(date +%M)*60+$(date +%S))) -fs -zoom /media/pi/<name-of-your-usb-stick-partition>/st24.mov
```
Make the script executable and create a folder for the autostart:
```bash
chmod +x st.sh
mkdir ~/.config/autostart
cd ~/.config/autostart
nano standardtime.desktop
```
Create a `.desktop` file for the video start script within the autostart folder. 
It looks like this:
```
[Desktop Entry]
Name=StandardTime
Exec=/home/pi/st.sh
Type=application
```
Restart your Raspberry Pi. The clock video is going to be played as soon as the 
boot up process is finished. 

## Known Issues

- In my setup the video does not play in real time. Each hour it will be a few minutes behind.
- There seems to be no way to stop the clock without using the USB keyboard (I use a touch screen). I consider this as feature because so nobody can interrupt the video presentation.
- Feel free to improve or correct this guide by providing merge requests or open an issue. Feedback welcome!

## Resources

- [Standard Time Project Page](https://www.standard-time.com/about_en.php)
- [A Guide how to set up a virtual RaspberryPie using VirtualBox](https://grantwinney.com/how-to-create-a-raspberry-pi-virtual-machine-vm-in-virtualbox/)
- [LCD display page](http://www.lcdwiki.com/5inch_HDMI_Display)

## My other Raspberry Pi project

- [RaspiLMS](https://gitlab.com/jukey/raspilms) - Make your Raspberry running a logitech media server extended by an independently created Spotify Plugin in order to serve Squeezebox devices
